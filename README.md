Prérequis : 
git doit etre installé et fonctionnel
docker doit être installé et fonctionnel
docker-compose doit être installé et fonctionnel

---------

Instructions : 
Clonez le repo par :
git clone https://framagit.org/rl-tests-et-cours/PatriceG.git

Se rendre dans le dossier cree via :
cd PatriceG

lancez la commande :
docker-compose up -d

via un navigateur web visitez les sites : 
http://first.localhost:81
ou
http://localhost:81
pour interroger directement le container whoami1

http://second.localhost:82
ou 
http://localhost:82
pour interroger directement le container whoami2

Puis, pour idemtifier que traefik est bien un reverse-proxy, interogez les URL : 
http://whoami1.docker.com
et
http://whoami2.docker.com

Vous pouvez bien entendu vérifier le monitoring de traefik sur cet URL : 
http://localhost:8080


